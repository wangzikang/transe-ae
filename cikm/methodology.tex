\section{Methodology}

\input{figOverall}

To learn from multimodal knowledge and structural knowledge jointly,
we first get knowledge from each single modality by learning feature vectors for each modality respectively,
Then we feed these feature vectors into multimodal autoencoder.
For entities, we let the middle-hidden layer of autoencoder satisfy the TransE assumption with corresponding relations.
The overall architecture of TransAE is shown in Figure \ref{overall}.


\subsection{Knowledge Extraction}  

We use feature vectors of images and text as visual and textual knowledge respectively. 
For visual knowledge, we use VGG16 Net\cite{DBLP:journals/corr/SimonyanZ14a} pre-trained on ImageNet\cite{deng2009imagenet}.
VGG16 Net consists of several convolution layers each followed by a ReLU layer and a max pooling layer,
there are two fully connected layers before the final softmax layer classifying images into $1000$ categories. 
We use the vector from the last fully connected layer as our desired image feature vector. 

To extract textual features, we use PV-DM\cite{pmlr-v32-le14} model,
one of the Doc2Vec\cite{pmlr-v32-le14} models, to learn paragraph vectors in an unsupervised way.
PV-DM can learn a fixed-length representation from variable-length sentences,
which is the definition of entities in our case.
PV-DM model is like Word2Vec\cite{mikolov2013distributed} model,
it maps the paragraph to a vector,
then calculate a joint vector by average or concatenate the paragraph vector with word vectors,
and predict the next word according to the joint vector.
Compared to Word2Vec model, Doc2Vec model also take the word order into consideration,
encoding both semantic knowledge and context information into embeddings at the same time.

\subsection{Model} 
\textbf{Multimodal autoencoder}\\
Multimodal autoencoder we used in our paper is a feedforward neural network consisting of an input layer,
three hidden layers and a output layer.
The input layer takes input from two different modalities,
which are image feature vectors $\imageInput$ and text feature vectors $\textInput$ we get in the previous initialization step.
A fully-connected hidden layer with much less units is followed the input layer for each modality,
we map the input to these hidden layers seperately,
and denote them as $\imageLatent$ and $\textLatent$.
Then we concatenate $\imageLatent$ and $\textLatent$,
map it to the second hidden layer jointly,
which is also the middle layer of autoencoder network,
and the desired joint multimodal entity embedding $\jointLatent$.

The decoder stage has exactly symmetrical structure with encoder,
taking the embedding $\jointLatent$ as input,
first map it to two seperate hidden layers $\imageLatentDecoder$ and $\textLatentDecoder$,
each shares the same dimension with the corresponding hidden layer in the encoder part,
$\imageLatent$ and $\textLatent$.
Then map the two layers to output layer $\imageOutput$, $\textOutput$.
Output layer and input layer are of same dimension for each modality.
Output layer is called reconstruction layer, which aims to reconstruct the input feature vectors.

The whole architecture of multimodal autoencoder is defined as:

\begin{align}
&\imageLatent = f(\weightsImgLatent \times \imageInput + \biasImgLatent) \\
&\textLatent = f(\weightsTxtLatent \times \textInput + \biasTxtLatent) \\
&\jointLatent = f(\weightsLatent \times (\imageLatent \oplus \textLatent) + \biasLatent)\\
&\imageLatentDecoder = f(\weightsImgLatentDecoder \times \jointLatent + \biasImgLatentDecoder) \\
&\textLatentDecoder = f(\weightsTxtLatentDecoder \times \jointLatent + \biasTxtLatentDecoder) \\
&\imageOutput = f(\weightsImgOutput \times \imageLatentDecoder + \biasImgOutput) \\
&\textOutput = f(\weightsTxtOutput \times \textLatentDecoder + \biasTxtOutput)
\label{eqn: ae}
\end{align}

\noindent where $f$ is an activation function, which is set to sigmoid in our paper.
$W_i^{(j)}$ and $W_t^{(j)}$ denote the weight matrix mapping embeddings from layer $j$ to layer $(j+1)$ of modality image and text respectively,
$b_i^{(j)}$ and $b_t^{(j)}$ are the bias items for image and text in the $j$-th hidden layer.
Symbol $\oplus$ represents the concatenate operation.

The whole multimodal autoencoder is trained to minimize reconstruction errors,
which is the sum of dissimilarities between input layer and output layer for two modalities:

\begin{align}
\aeloss = \left\|\imageInput - \imageOutput\right\|_2^2 
        + \left\|\textInput - \textOutput\right\|_2^2
\end{align}

\noindent \textbf{TransE}\\
TransE model maps entities and relations in low-dimensional continuous vector space, where embeddings of head entity $\head$, tail entity $\tail$, and relation $\relation$ satisfy:

\begin{align}
\head + \relation \approx \tail
\end{align}

In TransE, we try to minimize the distance between $\head + \relation$ and $\tail$ for triplets in knowledge graph, and maximize distance for corrupted ones we construct on our own. While training TransE, we minimize loss $\transeloss$:

\begin{equation}
\begin{split}
\transeloss =& \sum_{(\head, \relation, \tail)\in S} \sum_{(\head', \relation', \tail')\in S'}\max(0, [\gamma + \\
&d(\head, \relation, \tail)-d(\head', \relation', \tail')])
\label{eqn: loss}
\end{split}
\end{equation}


\noindent where $\gamma$ is a margin hyperparameter, and $\max(0, x)$ aims to get the positive part of $x$, $d$ is the dissimilarity function, can either be L1 norm or L2 norm. $S$ denote the original dataset consists of golden triplets existed in knowledge graph, while $S'$ is the corrupted dataset constructed by replacing head or tail entity for each triplet, following:

\begin{align}
S'_{\head, \relation, \tail}={(\corruptedHead, \relation, \tail)|\corruptedHead \in \entitySet } \cup {(\head, \relation, \corruptedTail)|\corruptedTail \in \entitySet }
\end{align}


\noindent \textbf{\modelName}\\
In our TransAE model, we combine the above two models to learn from multimodal knowledge and structural knowledge simultaneously.
There are several images and a sentence description for each entity in the database,
we first extract their visual and textual feature vectors, then feed these into
multimodal autoencoder to get the joint embedding as the entity representation.
Relation embeddings are initialized randomly in the beginning of the training. 
These entity and relation representations are used to train our model. 
Structure loss $\transeloss^{'}$ for $(\head, \relation, \tail)$ can be represented as:

\begin{equation}
\begin{split}
\transeloss' =& \sum_{(\head, \relation, \tail)\in S} \sum_{(\head', \relation', \tail')\in S'}\max(0, [\gamma + \\
& d(\jointLatent_{\head}, \relation, \jointLatent_{\tail})-d(\jointLatent_{\head'}, \relation', \jointLatent_{\tail'})])
\end{split}
\end{equation}

\noindent where $\jointLatent_{\head}$ and $\jointLatent_{\tail}$ represents the representation for $\head$ and $\tail$ embeddings respectively. 
For better generalization, we also add a regularizer item $\regularizer$  for parameter set $\theta$, with $\weightsRegularizer$ as its weight.
Thus we can train our model by minimizing the overall loss $\loss$: 

\begin{align}
L = \aeloss + \weightsaeloss\transeloss' + \weightsRegularizer\regularizer
\end{align}

Parameters $W_i^{(j)}$, $W_t^{(j)}$,  $b_i^{(j)}$, $b_t^{(j)}$ in autoencoder and relation embeddings are initialized randomly at the beginning of training. 
Weight parameters $\weightsaeloss$ and $\weightsRegularizer$ are chosen to balance the magnitude and importance of losses $\aeloss$, $\transeloss'$ and regularizer $\regularizer$.

At each round during training, a set of triplets are randomly selected, combing with the constructed corrputed triplets, together serve as batch data. 
We train our model on the training set and select optimal parameters based on the validation dataset. 
Performance of our model is illustrated in the next section in detail.


