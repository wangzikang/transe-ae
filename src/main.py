'''
 transE-ae

 main.py:
     main function
'''

import tensorflow as tf
import numpy as np
import argparse
import time
import math
import random
import os

from transE import TransE
from dataUtil import DataUtil


class Config(object):

    def __init__(self):
        pass

    def __str__(self):
        s = """
Parameters:
batch size: {:d}
learning rate: {:.3f}
margin: {:.3f}
dissimilarity measure is L1: {}
is bernoulli: {}
get image vector: {}
weight of autoencoder loss: {:.3f}

Dimensions:
input image dimension: {:d}
input text dimension: {:d}
latent layer1 image dimension: {:d}
latent layer1 text dimension: {:d}
latent dimension: {:d}
            """.format(self.batch_size, self.learning_rate, self.margin, self.L1_norm, self.bern, self.img_vec, self.weight_loss, self.num_img_input, self.num_txt_input, self.num_hidden_1_img, self.num_hidden_1_txt, self.dimension)
        return s

def train(model, data):
    graph = tf.Graph()
    with graph.as_default():

        # construct graph
        print('constructing the training graph...')

        with tf.variable_scope('input'):
            train_triple_positive_input = tf.placeholder(
                dtype=tf.int32,
                shape=[model.batch_size, 3],
                name='triplets_positive'
            )
            train_triple_negative_input = tf.placeholder(
                dtype=tf.int32,
                shape=[model.batch_size, 3],
                name='triplets_negative'
            )
            triplets_predict_head = tf.placeholder(
                dtype=tf.int32,
                shape=[None, 3],
                name='triplets_predict_head'
            )
            triplets_predict_tail = tf.placeholder(
                dtype=tf.int32,
                shape=[None, 3],
                name='triplets_predict_tail'
            )

        bound = 6 / math.sqrt(model.dimension)
        bound_ae = 0.45 
        # bound_ae = bound 
		
        with tf.variable_scope('autoencoder'):
            weights = {
                'encoder_m1_h1': tf.get_variable(
				    name='encoder_m1_h1',
				    # initializer=tf.random_normal(
			            initializer=tf.random_uniform(
					    shape=[model.num_img_input, model.num_hidden_1_img],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'encoder_m2_h1': tf.get_variable(
				    name='encoder_m2_h1',
					# initializer=tf.random_normal(
					initializer=tf.random_uniform(
					    shape=[model.num_txt_input, model.num_hidden_1_txt],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'encoder_h1_h2': tf.get_variable(
				    name='encoder_h1_h2',
					# initializer=tf.random_normal(
					initializer=tf.random_uniform(
				        shape=[model.num_hidden_1_img+model.num_hidden_1_txt, model.num_hidden_2],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'decoder_m1_h1': tf.get_variable(
				    name='decoder_m1_h1',
					# initializer=tf.random_normal(
					initializer=tf.random_uniform(
				        shape=[model.num_hidden_2, model.num_hidden_1_img],
                                        minval=-bound_ae,
                                        maxval=bound_ae,
					)
				),
                'decoder_m2_h1': tf.get_variable(
				    name='decoder_m2_h1',
					initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_hidden_2, model.num_hidden_1_txt],    
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'decoder_m1_h2': tf.get_variable(
				    name='decoder_m1_h2',
					initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_hidden_1_img, model.num_img_input],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'decoder_m2_h2': tf.get_variable(
				    name='decoder_m2_h2',
                                        initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_hidden_1_txt, model.num_txt_input],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				)
            }

            biases = {
                'encoder_m1_b1': tf.get_variable(
				    name='encoder_m1_b1',
                                        initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_hidden_1_img],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'encoder_m2_b1': tf.get_variable(
				    name='encoder_m2_b1',
                                        initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_hidden_1_txt],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'encoder_b2': tf.get_variable(
				    name='encoder_b2',
                                    initializer=tf.random_uniform(
                                        # initializer=tf.random_normal(
					    shape=[model.num_hidden_2],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'decoder_m1_b1': tf.get_variable(
				    name='decoder_m1_b1',
                                        initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_hidden_1_img],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'decoder_m2_b1': tf.get_variable(
				    name='decoder_m2_b1',
                                        initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_hidden_1_txt],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'decoder_m1_b2': tf.get_variable(
				    name='decoder_m1_b2',
                                        initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_img_input],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				),
                'decoder_m2_b2': tf.get_variable(
				    name='decoder_m2_b2',
                                        initializer=tf.random_uniform(
					# initializer=tf.random_normal(
					    shape=[model.num_txt_input],
                                            minval=-bound_ae,
                                            maxval=bound_ae,
					)
				)
            }
		
        with tf.variable_scope('rel_embedding'):
            '''
            embedding_entity = tf.get_variable(
                name='entity',
                initializer=tf.random_uniform(
                    shape=[data.num_entity, model.dimension]
                )
            )
		    '''
            embedding_relation = tf.get_variable(
                name='relation',
                initializer=tf.random_uniform(
                    shape=[data.num_relation, model.dimension],
                    minval=-bound,
                    # minval=0,
                    maxval=bound
                )
            )

        # TODO: attention
        with tf.name_scope('ent_embedding'):
            embedding_img = tf.constant(data.get_img_data(), name='img')
            embedding_txt = tf.constant(data.get_txt_data(), name='txt')

        with tf.name_scope('normalization'):
            normalize_relation_op = embedding_relation.assign(tf.clip_by_norm(embedding_relation, clip_norm=1, axes=1))

        with tf.name_scope('inference'):
            d_positive, d_negative, d_ae_pos, d_ae_neg, d_s_p, d_l_p, l2_norm_p, d_s_n, d_l_n, l2_norm_n = model.inference(
			    train_triple_positive_input, train_triple_negative_input)

        with tf.name_scope('loss'):
            # structure_loss, clip_loss, norm_loss, ae_loss, loss, dissimilarity = model.loss(d_positive, d_negative, d_ae_pos, d_ae_neg, d_s_p, d_l_p, l2_norm_p, d_s_n, d_l_n, l2_norm_n)
            structure_loss, clip_loss, norm_loss, ae_loss, loss = model.loss(d_positive, d_negative, d_ae_pos, d_ae_neg, d_s_p, d_l_p, l2_norm_p, d_s_n, d_l_n, l2_norm_n)

        with tf.name_scope('optimization'):
            train_op = model.train(loss)

        with tf.name_scope('evaluation'):
            predict_head, predict_tail = model.evaluate(triplets_predict_head, triplets_predict_tail)

        print('graph construction finished')

        init_op = tf.global_variables_initializer()
        merge_summary_op = tf.summary.merge_all()
        saver = tf.train.Saver()

    # open a session
    # os.environ["CUDA_VISIBLE_DEVICES"] = '0'
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    session_config = tf.ConfigProto(log_device_placement=True)
    session_config.gpu_options.per_process_gpu_memory_fraction = 0.6
    session_config.gpu_options.allow_growth = True

    with tf.Session(graph=graph, config=session_config) as sess:
        print('initializing all variables...')
        sess.run(init_op)
        print('all variables initialized')

        sess.run(normalize_relation_op)

        summary_writer = tf.summary.FileWriter(data.result_folder, graph=sess.graph)

        num_batch = data.num_train_triple // model.batch_size

        print("Start trianing...")
        start_total = time.time()

        for epoch in range(model.num_epoch):
            loss_epoch = 0
            start_epoch = time.time()
            for batch in range(num_batch):

                batch_positive, batch_negative = data.next_batch_train(model.batch_size)
				
                feed_dict_train = {
                    train_triple_positive_input: batch_positive,
                    train_triple_negative_input: batch_negative
                }

                # _, loss_batch, summary = sess.run([train_op, loss, merge_summary_op], feed_dict=feed_dict_train)
                _, structureLoss, clipLoss, normLoss, aeLoss, loss_batch, summary = sess.run([train_op, structure_loss, clip_loss, norm_loss, ae_loss, loss, merge_summary_op], feed_dict=feed_dict_train)
                loss_epoch += loss_batch

                # print("batch {}, loss {}, dis {}".format(batch, loss_batch, dis))
                # print("batch {}, loss {}".format(batch, loss_batch))
                # print('structure loss: {}, clip loss: {}'.format(structureLoss[:5], clipLoss[:5]))
                # print('norm loss: {}, ae loss: {}'.format(normLoss[:5], aeLoss[:5]))

                summary_writer.add_summary(summary, global_step=epoch*num_batch+batch)

                # if(batch + 1) % 10 == 0 or (batch + 1) == num_batch:
                    # print("epoch {}, batch{}, loss: {}".format(epoch, batch, loss_batch))

            end_epoch = time.time()
            print("epoch {} finished. mean batch loss: {:.3f}, time used: {:.3f}s".format(epoch, loss_epoch / num_batch, end_epoch - start_epoch))

            # save a checkpoint every epoch
            save_path = saver.save(sess, os.path.join(data.result_folder, data.result_file))
            print('model save in file {}'.format(os.path.join(data.result_folder, 'model.ckpt')))

            # evaluate every 10 epochs
            if (epoch + 1) % 50 == 0:
                evaluate(sess, data, data.valid_triple, data.num_valid_triple, model, predict_head, predict_tail, triplets_predict_head, triplets_predict_tail)

            if (epoch + 1) % 50 == 0:
                ent_embedding = model.encoder(embedding_img, embedding_txt).eval()
                print(ent_embedding[:3])


        end_total = time.time()
        print("training finished. Total time: {:.3f}s".format(end_total - start_epoch))

        print("----Experiment 1: rank & hit----")
        evaluate(sess, data, data.test_triple, data.num_test_triple, model, predict_head, predict_tail, triplets_predict_head, triplets_predict_tail)
		
        print("----Experiment 2: Classification----")
        classification(sess, data, model, predict_head, predict_tail, triplets_predict_head, triplets_predict_tail)

		

def evaluate(sess, data, data_to_evaluate, evaluate_size, model, predict_head, predict_tail, triplets_predict_head, triplets_predict_tail):
    print("evaluating the model...")
    start_eval = time.time()

    rank_head_raw = 0
    rank_tail_raw = 0
    hit10_head_raw = 0
    hit10_tail_raw = 0

    rank_head_filter = 0
    rank_tail_filter = 0
    hit10_head_filter = 0
    hit10_tail_filter = 0

    for triplet in data_to_evaluate:
    # for triplet in random.sample(data_to_evaluate, evaluate_size):
        batch_predict_head_raw, batch_predict_head_filter, batch_predict_tail_raw, batch_predict_tail_filter = data.next_batch_eval(triplet)

        # raw
        feed_dict_eval_raw = {
            triplets_predict_head: batch_predict_head_raw,
            triplets_predict_tail: batch_predict_tail_raw
        }
        # rank list of head and tail prediction
        prediction_head_raw, prediction_tail_raw = sess.run([predict_head, predict_tail], feed_dict=feed_dict_eval_raw)
        # print('prediction_head_raw: {}, prediction_tail_raw: {}'.format(prediction_head_raw, prediction_tail_raw))

        rank_head_current_raw = prediction_head_raw.argsort().argmin()
        rank_head_raw += rank_head_current_raw
        if rank_head_current_raw < 10:
            hit10_head_raw += 1

        rank_tail_current_raw = prediction_tail_raw.argsort().argmin()
        rank_tail_raw += rank_tail_current_raw
        if rank_tail_current_raw < 10:
            hit10_tail_raw += 1

        # filter
        feed_dict_eval_filter = {
            triplets_predict_head: batch_predict_head_filter,
            triplets_predict_tail: batch_predict_tail_filter
        }

        prediction_head_filter, prediction_tail_filter = sess.run([predict_head, predict_tail], feed_dict=feed_dict_eval_filter)

        rank_head_current_filter = prediction_head_filter.argsort().argmin()
        rank_head_filter += rank_head_current_filter
        if rank_head_current_filter < 10:
            hit10_head_filter += 1

        rank_tail_current_filter = prediction_tail_filter.argsort().argmin()
        rank_tail_filter += rank_tail_current_filter
        if rank_tail_current_filter < 10:
            hit10_tail_filter += 1
    
    end_eval = time.time()

    rank_head_mean_raw = rank_head_raw // evaluate_size
    rank_tail_mean_raw = rank_tail_raw // evaluate_size
        
    hit10_head_raw /= evaluate_size
    hit10_tail_raw /= evaluate_size
        
    print('head prediction raw mean rank: {:d},raw hit@10: {:.3f}%'.format(rank_head_mean_raw, hit10_head_raw * 100))
    print('tail prediction raw mean rank: {:d},raw hit@10: {:.3f}%'.format(rank_tail_mean_raw, hit10_tail_raw * 100))

    rank_head_mean_filter = rank_head_filter // evaluate_size
    rank_tail_mean_filter = rank_tail_filter // evaluate_size

    hit10_head_filter /= evaluate_size
    hit10_tail_filter /= evaluate_size

    print('head prediction filtered mean rank: {:d},filtered hit@10: {:.3f}%'.format(rank_head_mean_filter, hit10_head_filter * 100))
    print('tail prediction filtered mean rank: {:d},filtered hit@10: {:.3f}%'.format(rank_tail_mean_filter, hit10_tail_filter * 100))

    print('time used during evaluation: {:.3f}s'.format(end_eval - start_eval))

    print('evaluation ended.')

def classification(sess, data, model, predict_head, predict_tail, triplets_predict_head, triplets_predict_tail):
    print('classifying...')

    num_correct = 0
	
    rel2thred = {}
    rel2d_pos = {}
    rel2d_neg = {}
    rel2d_num = {}
	
    for rel_id in data.rel_id:
        rel2thred[rel_id] = 0
        rel2d_pos[rel_id] = 0
        rel2d_neg[rel_id] = 0
        rel2d_num[rel_id] = 0
		
    for triplet in data.valid_triple:
        (h_id, r_id, t_id) = triplet
        rel2d_num[r_id] += 1
	
        classification_triplet_pos = [triplet]
        classification_triplet_neg = [data.get_corrupted_data(triplet)]
		
        feed_dict_classification = {
            triplets_predict_head: classification_triplet_pos,
            triplets_predict_tail: classification_triplet_neg
        }
		
        d_pos, d_neg = sess.run([predict_head, predict_tail], feed_dict=feed_dict_classification)
		
        rel2d_pos[r_id] += d_pos[0]
        rel2d_neg[r_id] += d_neg[0]
			
    for rel_id in data.rel_id:
        rel2thred[rel_id] = (rel2d_pos[rel_id] + rel2d_neg[rel_id]) / (2 * rel2d_num[rel_id])
		
    for triplet in data.test_triple:
        classification_test_triplet_pos = [triplet]
        classification_test_triplet_neg = [data.get_corrupted_data(triplet)]
		
        feed_dict_classification_test = {
		    triplets_predict_head: classification_test_triplet_pos,
			triplets_predict_tail: classification_test_triplet_neg
		}
		
        d_test_pos, d_test_neg = sess.run([predict_head, predict_tail], feed_dict=feed_dict_classification_test)
		
        if d_test_pos[0] <= rel2thred[r_id]:
            num_correct += 1
        if d_test_neg[0] >= rel2thred[r_id]:
            num_correct += 1

    accuracy = num_correct / (2 * data.num_test_triple)
    print('Classification completed! The accuracy is {:.3f}%'.format(accuracy * 100))



def main():

    parser = argparse.ArgumentParser(description = "TransE model")
    
    # TODO: write help
    # TODO: result file
    parser.add_argument("-d", "--data_directory", dest="data_directory", type=str, default="../data/", help="")
    parser.add_argument("-r", "--result_directory", dest="result_directory", type=str, default="../result/", help="")
    parser.add_argument("--ent2id_name", dest="ent2id_name", type=str, default="entity2id.txt", help="")
    parser.add_argument("--rel2id_name", dest="rel2id_name", type=str, default="relation2id.txt", help="")
    parser.add_argument("--train_name", dest="train_name", type=str, default="train.txt", help="")
    parser.add_argument("--valid_name", dest="valid_name", type=str, default="valid.txt", help="")
    parser.add_argument("--test_name", dest="test_name", type=str, default="test.txt", help="")
    parser.add_argument("--image_list", dest="image_list", type=str, default="image_list.txt", help="")
    parser.add_argument("--image2count", dest="image2count", type=str, default="image2count.txt", help="")
    parser.add_argument("--img_id2vec", dest="img_id2vec", type=str, default="img.img2vec", help="")
    parser.add_argument("--img_index_scale", dest="img_index_scale", type=str, default="image_index_scale.txt", help="")
    parser.add_argument("--img_vec", dest="img_vec", type=str, default="average", help="")
    parser.add_argument("--doc2vec", dest="doc2vec", type=str, default="definition.doc2vec", help="")
    parser.add_argument("--num_epoch", dest="num_epoch", type=int, default=1000, help="")
    parser.add_argument("--batch_size", dest="batch_size", type=int, default=500, help="")
    parser.add_argument("-lr", "--learning_rate", dest="learning_rate", type=float, default=0.0005, help="")
    parser.add_argument("-m", "--margin", dest="margin", type=float, default=0.5, help="")
    parser.add_argument("--num_img_input", dest="num_img_input", type=int, default=4096, help="")
    parser.add_argument("--num_txt_input", dest="num_txt_input", type=int, default=100, help="")
    parser.add_argument("--num_hidden_1_img", dest="num_hidden_1_img", type=int, default=512, help="")
    parser.add_argument("--num_hidden_1_txt", dest="num_hidden_1_txt", type=int, default=50, help="")
    parser.add_argument("--dimension", dest="dimension", type=int, default=20, help="")
    parser.add_argument("--L1_norm", dest="L1_norm", type=bool, default=True, help="")
    parser.add_argument("-b", "--bern", dest="bern", type=bool, default=False, help="")
    parser.add_argument("-v", "--verbosity", dest="verbosity", type=int, default=3, help="")
    parser.add_argument("-o", "--optimizer", dest="optimizer", type=str, default="rms", help="")
    # TODO: not sure
    parser.add_argument("-w", "--regularizer_weight", dest="regularizer_weight", type=float, default=1.0, help="")
    parser.add_argument( "--evaluate_size", dest="evaluate_size", type=int, default=50, help="")
    parser.add_argument("--weight_loss", dest="weight_loss", type=float, default=0.5, help="")
    parser.add_argument("--result_file", dest="result_file", type=str, default="default.ckpt", help="")
    parser.add_argument("--evaluate_file", dest="evaluate_file", type=str, default="default.ckpt", help="")
    
    args = parser.parse_args()
    
    config = Config()

    config.ent2id_file = os.path.join(args.data_directory, args.ent2id_name)
    config.rel2id_file = os.path.join(args.data_directory, args.rel2id_name)
    config.train_file = os.path.join(args.data_directory, args.train_name)
    config.valid_file = os.path.join(args.data_directory, args.valid_name)
    config.test_file = os.path.join(args.data_directory, args.test_name)
    config.image_list_file = os.path.join(args.data_directory, args.image_list)
    config.image2count_file = os.path.join(args.data_directory, args.image2count)
    config.img_id2vec_file = os.path.join(args.data_directory, args.img_id2vec)
    config.img_index_scale_file = os.path.join(args.data_directory, args.img_index_scale)
    config.img_vec = args.img_vec
    config.doc2vec_file = os.path.join(args.data_directory, args.doc2vec)
    config.result_folder = args.result_directory
    config.num_epoch = args.num_epoch
    config.batch_size = args.batch_size
    config.learning_rate = args.learning_rate
    config.margin = args.margin
    config.num_img_input = args.num_img_input
    config.num_txt_input = args.num_txt_input
    config.num_hidden_1_img = args.num_hidden_1_img
    config.num_hidden_1_txt = args.num_hidden_1_txt
    config.dimension = args.dimension
    config.L1_norm = args.L1_norm
    config.bern = args.bern
    config.verbosity = args.verbosity
    config.optimizer = args.optimizer
    config.regularizer_weight = args.regularizer_weight
    config.evaluate_size = args.evaluate_size
    config.weight_loss = args.weight_loss
    config.result_file = args.result_file
    config.evaluate_file = args.evaluate_file

    if config.verbosity > 0:
        print(config)

    transe_model = TransE(config)
    data = DataUtil(config)
    data.load_data()
    train(transe_model, data)



if __name__ == "__main__":
    main()

