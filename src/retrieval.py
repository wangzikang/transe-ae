'''
 transE-ae

 retrieval.py:
     find the nearest entity in embedding space
'''

import tensorflow as tf
import numpy as np
import argparse
import time
import math
import random
import os
from sklearn.neighbors import KDTree

from transE import TransE
from dataUtil import DataUtil

class Config(object):

    def __init__(self):
        pass

    def __str__(self):
        # TODO: fill this
        s = """
Parameters:
batch size: {:d}
learning rate: {:.3f}
margin: {:.3f}
dissimilarity measure is L1: {}
is bernoulli: {}
get image vector: {}
weight of autoencoder loss: {:.3f}

Dimensions:
input image dimension: {:d}
input text dimension: {:d}
latent layer1 image dimension: {:d}
latent layer1 text dimension: {:d}
latent dimension: {:d}
            """.format(self.batch_size, self.learning_rate, self.margin, self.L1_norm, self.bern, self.img_vec, self.weight_loss, self.num_img_input, self.num_txt_input, self.num_hidden_1_img, self.num_hidden_1_txt, self.dimension)
        return s

def get_ent_embedding(model, data):
    graph = tf.Graph()
    with graph.as_default():

        # construct graph
        print('constructing the training graph...')

        with tf.variable_scope('input'):
            train_triple_positive_input = tf.placeholder(
                dtype=tf.int32,
                shape=[model.batch_size, 3],
                name='triplets_positive'
            )
            train_triple_negative_input = tf.placeholder(
                dtype=tf.int32,
                shape=[model.batch_size, 3],
                name='triplets_negative'
            )
            triplets_predict_head = tf.placeholder(
                dtype=tf.int32,
                shape=[None, 3],
                name='triplets_predict_head'
            )
            triplets_predict_tail = tf.placeholder(
                dtype=tf.int32,
                shape=[None, 3],
                name='triplets_predict_tail'
            )

        bound = 6 / math.sqrt(model.dimension)
		
        with tf.variable_scope('autoencoder'):
            weights = {
                'encoder_m1_h1': tf.get_variable(
				    name='encoder_m1_h1',
				    initializer=tf.random_normal(
					    shape=[model.num_img_input, model.num_hidden_1_img]
					)
				),
                'encoder_m2_h1': tf.get_variable(
				    name='encoder_m2_h1',
					initializer=tf.random_normal(
					    shape=[model.num_txt_input, model.num_hidden_1_txt]
					)
				),
                'encoder_h1_h2': tf.get_variable(
				    name='encoder_h1_h2',
					initializer=tf.random_normal(
				        shape=[model.num_hidden_1_img+model.num_hidden_1_txt, model.num_hidden_2]
					)
				),
                'decoder_m1_h1': tf.get_variable(
				    name='decoder_m1_h1',
					initializer=tf.random_normal(
				        shape=[model.num_hidden_2, model.num_hidden_1_img]
					)
				),
                'decoder_m2_h1': tf.get_variable(
				    name='decoder_m2_h1',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_2, model.num_hidden_1_txt]
					)
				),
                'decoder_m1_h2': tf.get_variable(
				    name='decoder_m1_h2',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_1_img, model.num_img_input]
					)
				),
                'decoder_m2_h2': tf.get_variable(
				    name='decoder_m2_h2',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_1_txt, model.num_txt_input]
					)
				)
            }

            biases = {
                'encoder_m1_b1': tf.get_variable(
				    name='encoder_m1_b1',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_1_img]
					)
				),
                'encoder_m2_b1': tf.get_variable(
				    name='encoder_m2_b1',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_1_txt]
					)
				),
                'encoder_b2': tf.get_variable(
				    name='encoder_b2',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_2]
					)
				),
                'decoder_m1_b1': tf.get_variable(
				    name='decoder_m1_b1',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_1_img]
					)
				),
                'decoder_m2_b1': tf.get_variable(
				    name='decoder_m2_b1',
					initializer=tf.random_normal(
					    shape=[model.num_hidden_1_txt]
					)
				),
                'decoder_m1_b2': tf.get_variable(
				    name='decoder_m1_b2',
					initializer=tf.random_normal(
					    shape=[model.num_img_input]
					)
				),
                'decoder_m2_b2': tf.get_variable(
				    name='decoder_m2_b2',
					initializer=tf.random_normal(
					    shape=[model.num_txt_input]
					)
				)
            }
		
        with tf.variable_scope('rel_embedding'):
            embedding_relation = tf.get_variable(
                name='relation',
                initializer=tf.random_uniform(
                    shape=[data.num_relation, model.dimension]
                )
            )

        with tf.name_scope('ent_embedding'):
            embedding_img = tf.constant(data.get_img_data(), name='img')
            embedding_txt = tf.constant(data.get_txt_data(), name='txt')

        with tf.name_scope('normalization'):
            normalize_relation_op = embedding_relation.assign(tf.clip_by_norm(embedding_relation, clip_norm=1, axes=1))

        with tf.name_scope('inference'):
            d_positive, d_negative, d_ae_pos, d_ae_neg = model.inference(
			    train_triple_positive_input, train_triple_negative_input)

        with tf.name_scope('loss'):
            loss = model.loss(d_positive, d_negative, d_ae_pos, d_ae_neg)

        with tf.name_scope('optimization'):
            train_op = model.train(loss)

        with tf.name_scope('evaluation'):
            predict_head, predict_tail = model.evaluate(triplets_predict_head, triplets_predict_tail)

        print('graph construction finished')

        init_op = tf.global_variables_initializer()
        merge_summary_op = tf.summary.merge_all()
        saver = tf.train.Saver()

    # open a session
    session_config = tf.ConfigProto(log_device_placement=True)
    session_config.gpu_options.allow_growth = True
    with tf.Session(graph=graph, config=session_config) as sess:
    with tf.Session(graph=graph) as sess:
        print('initializing all variables...')
        sess.run(init_op)
        print('all variables initialized')

        sess.run(normalize_relation_op)

        summary_writer = tf.summary.FileWriter(data.result_folder, graph=sess.graph)

        num_batch = data.num_train_triple // model.batch_size
		
        saver.restore(sess, os.path.join(data.result_folder, data.evaluate_file))
        ent_embedding = model.encoder(embedding_img, embedding_txt).eval()

    return ent_embedding


def main():
    parser = argparse.ArgumentParser(description = "TransE model")
    
    # TODO: write help
    # TODO: result file
    parser.add_argument("-d", "--data_directory", dest="data_directory", type=str, default="../data/", help="")
    parser.add_argument("-r", "--result_directory", dest="result_directory", type=str, default="../result/", help="")
    parser.add_argument("--ent2id_name", dest="ent2id_name", type=str, default="entity2id.txt", help="")
    parser.add_argument("--rel2id_name", dest="rel2id_name", type=str, default="relation2id.txt", help="")
    parser.add_argument("--train_name", dest="train_name", type=str, default="train.txt", help="")
    parser.add_argument("--valid_name", dest="valid_name", type=str, default="valid.txt", help="")
    parser.add_argument("--test_name", dest="test_name", type=str, default="test.txt", help="")
    parser.add_argument("--image_list", dest="image_list", type=str, default="image_list.txt", help="")
    parser.add_argument("--image2count", dest="image2count", type=str, default="image2count.txt", help="")
    parser.add_argument("--img_id2vec", dest="img_id2vec", type=str, default="img.img2vec", help="")
    parser.add_argument("--img_index_scale", dest="img_index_scale", type=str, default="image_index_scale.txt", help="")
    parser.add_argument("--img_vec", dest="img_vec", type=str, default="average", help="")
    parser.add_argument("--doc2vec", dest="doc2vec", type=str, default="definition.doc2vec", help="")
    parser.add_argument("--num_epoch", dest="num_epoch", type=int, default=4000, help="")
    parser.add_argument("--batch_size", dest="batch_size", type=int, default=1000, help="")
    parser.add_argument("-lr", "--learning_rate", dest="learning_rate", type=float, default=0.01, help="")
    parser.add_argument("-m", "--margin", dest="margin", type=float, default=1.0, help="")
    parser.add_argument("--num_img_input", dest="num_img_input", type=int, default=4096, help="")
    parser.add_argument("--num_txt_input", dest="num_txt_input", type=int, default=100, help="")
    parser.add_argument("--num_hidden_1_img", dest="num_hidden_1_img", type=int, default=1024, help="")
    parser.add_argument("--num_hidden_1_txt", dest="num_hidden_1_txt", type=int, default=50, help="")
    parser.add_argument("--dimension", dest="dimension", type=int, default=50, help="")
    parser.add_argument("--L1_norm", dest="L1_norm", type=bool, default=False, help="")
    parser.add_argument("-b", "--bern", dest="bern", type=bool, default=True, help="")
    parser.add_argument("-v", "--verbosity", dest="verbosity", type=int, default=3, help="")
    parser.add_argument("-o", "--optimizer", dest="optimizer", type=str, default="gradient", help="")
    # TODO: not sure
    parser.add_argument("-w", "--regularizer_weight", dest="regularizer_weight", type=float, default=1.0, help="")
    parser.add_argument( "--evaluate_size", dest="evaluate_size", type=int, default=50, help="")
    parser.add_argument("--weight_loss", dest="weight_loss", type=float, default=0.5, help="")
    parser.add_argument("--result_file", dest="result_file", type=str, default="default.ckpt", help="")
    parser.add_argument("--evaluate_file", dest="evaluate_file", type=str, default="holder.ckpt", help="")
    
    args = parser.parse_args()
    
    config = Config()

    # TODO: file needed
    config.ent2id_file = os.path.join(args.data_directory, args.ent2id_name)
    config.rel2id_file = os.path.join(args.data_directory, args.rel2id_name)
    config.train_file = os.path.join(args.data_directory, args.train_name)
    config.valid_file = os.path.join(args.data_directory, args.valid_name)
    config.test_file = os.path.join(args.data_directory, args.test_name)
    config.image_list_file = os.path.join(args.data_directory, args.image_list)
    config.image2count_file = os.path.join(args.data_directory, args.image2count)
    config.img_id2vec_file = os.path.join(args.data_directory, args.img_id2vec)
    config.img_index_scale_file = os.path.join(args.data_directory, args.img_index_scale)
    config.img_vec = args.img_vec
    config.doc2vec_file = os.path.join(args.data_directory, args.doc2vec)
    config.result_folder = args.result_directory
    config.num_epoch = args.num_epoch
    config.batch_size = args.batch_size
    config.learning_rate = args.learning_rate
    config.margin = args.margin
    config.num_img_input = args.num_img_input
    config.num_txt_input = args.num_txt_input
    config.num_hidden_1_img = args.num_hidden_1_img
    config.num_hidden_1_txt = args.num_hidden_1_txt
    config.dimension = args.dimension
    config.L1_norm = args.L1_norm
    config.bern = args.bern
    config.verbosity = args.verbosity
    config.optimizer = args.optimizer
    config.regularizer_weight = args.regularizer_weight
    config.evaluate_size = args.evaluate_size
    config.weight_loss = args.weight_loss
    config.result_file = args.result_file
    config.evaluate_file = args.evaluate_file

    if config.verbosity > 0:
        print(config)

    transe_model = TransE(config)
    data = DataUtil(config)
    data.load_data()
    ent_embedding = get_ent_embedding(transe_model, data)

    tree = KDTree(ent_embedding)
    query_search = []

    for query_id in range(data.num_entity):
        # k=5 find the top 4
        _, id_list = tree.query([ent_embedding[query_id]], k=5)
        ent_list = [data.id2ent[id] for id in id_list[0].tolist()]
        query_search.append(ent_list)

    with open('../result/query_result.txt', 'w') as f:
        for q in query_search:
            f.write("{:s}: {:s}, {:s}, {:s}, {:s}\n".format(q[0], q[1], q[2], q[3], q[4]))


if __name__ == "__main__":
    main()

