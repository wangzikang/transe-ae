'''
 transE-ae

 dataUtil.py:
     arrange data
'''

import numpy as np
import random
import gensim
import random


class DataUtil(object):

    def __init__(self, config):
        self.ent2id_file = config.ent2id_file
        self.rel2id_file = config.rel2id_file
        self.train_file = config.train_file
        self.valid_file = config.valid_file
        self.test_file = config.test_file
        self.result_folder = config.result_folder
        self.result_file = config.result_file
        self.evaluate_file = config.evaluate_file
		
        self.image_list_file = config.image_list_file
        self.image2count_file = config.image2count_file
        self.img_id2vec_file = config.img_id2vec_file
        self.img_index_scale_file = config.img_index_scale_file
        self.doc2vec_file = config.doc2vec_file

        self.bern = config.bern
        self.img_vec = config.img_vec

        self.ent2id = {}
        self.id2ent = {}
        self.rel2id = {}
        self.id2rel = {}

        self.ent_id = []
        self.rel_id = []
    
        self.total_triple = set()
        self.train_triple = []
        self.valid_triple = []
        self.test_triple = []

        # dict_r_h: {r1:{h1:[t1, t2, ..], h2:[], ..}, r2: {}, ..} use too many memories
        self.dict_r_h = {}
        self.dict_r_t = {}
        
        self.corrupted_tail_triple = {}
        self.corrupted_head_triple = {}
		
        self.image_list = []
        self.image2count = {}
        self.img_id2vec = {}
        self.img_index_scale = []
		
        self.img_data = None
        self.txt_data = None

        self.num_entity = 0
        self.num_relation = 0
        self.num_train_triple = 0
        self.num_valid_triple = 0
        self.num_test_triple = 0


    def load_data(self):
        print("loading data...")
        
        with open(self.ent2id_file) as f:
            self.ent2id = {line.strip().split('\t')[0]: int(line.strip().split('\t')[1]) for line in f.readlines()}
            self.id2ent = {id: ent for ent, id in self.ent2id.items()}
            self.ent_id = [id for ent, id in self.ent2id.items()]
        
        with open(self.rel2id_file) as f:
            self.rel2id = {line.strip().split('\t')[0]: int(line.strip().split('\t')[1]) for line in f.readlines()}
            self.id2rel = {id: rel for rel, id in self.rel2id.items()}
            self.rel_id = [id for rel, id in self.rel2id.items()]
		
        self.img_data = np.loadtxt(self.img_id2vec_file)		
        self.txt_data = gensim.models.Doc2Vec.load(self.doc2vec_file)
        self.img_index_range = np.loadtxt(self.img_index_scale_file)

        def load_triple(self, triple_file):
            triple_data = []
            with open(triple_file) as f:
                for line in f.readlines():
                    line_list = line.strip().split('\t')
                    # if line_list[0] in self.ent2id and line_list[2] in self.rel2id and line_list[1] in self.ent2id:
                    h_id = self.ent2id[line_list[0]]
                    r_id = self.rel2id[line_list[2]]
                    t_id = self.ent2id[line_list[1]]
                    triple_data.append((h_id, r_id, t_id))

                    if r_id not in self.dict_r_h:
                        self.dict_r_h[r_id] = {h_id: [t_id]}
                    else:
                        if h_id not in self.dict_r_h[r_id]:
                            self.dict_r_h[r_id][h_id] = [t_id]
                        else:
                            self.dict_r_h[r_id][h_id].append(t_id)

                    if r_id not in self.dict_r_t:
                        self.dict_r_t[r_id] = {t_id: [h_id]}
                    else:
                        if t_id not in self.dict_r_t[r_id]:
                            self.dict_r_t[r_id][t_id] = [h_id]
                        else:
                            self.dict_r_t[r_id][t_id].append(h_id)
             
            return triple_data
                
        self.train_triple = load_triple(self, self.train_file)
        self.valid_triple = load_triple(self, self.valid_file)
        self.test_triple = load_triple(self, self.test_file)
        self.total_triple = set(map(tuple, np.concatenate([self.train_triple, self.valid_triple, self.test_triple], axis = 0)))
            
        self.num_entity = len(self.ent2id)
        self.num_relation = len(self.rel2id)
        self.num_train_triple = len(self.train_triple)
        self.num_valid_triple = len(self.valid_triple)
        self.num_test_triple = len(self.test_triple)

        print("entity number: {}".format(str(self.num_entity)))
        print("relation number: {}".format(str(self.num_relation)))
        print("train triple number: {}".format(str(self.num_train_triple)))
        print("validation triple number: {}".format(str(self.num_valid_triple)))
        print("test triple number: {}".format(str(self.num_test_triple)))

        print("Finish loading data.\n")

    def get_corrupted_data(self, triplet):
        (h, r, t) = triplet
        if self.bern == False:
            h_prob = np.random.binomial(1, 0.5)
        else:
            hpt = len(self.dict_r_t[r][t]) / len(self.dict_r_h[r][h])
            tph = len(self.dict_r_h[r][h]) / len(self.dict_r_t[r][t])
            h_prob = np.random.binomial(1, (tph / (tph + hpt)))

        while True:
            if h_prob:
                corrupted_triple = (random.sample(list(self.ent2id.values()), 1)[0], r, t)
            else:
                corrupted_triple = (h, r, random.sample(list(self.ent2id.values()), 1)[0])
            if corrupted_triple not in self.total_triple:
                break
        
        return corrupted_triple

    def next_batch_train(self, batch_size):
        batch_positive = random.sample(self.train_triple, batch_size)

        batch_negative = []
        for triplet in batch_positive:
            negative_sample = self.get_corrupted_data(triplet)
            batch_negative.append(negative_sample)
        
        return batch_positive, batch_negative

    def next_batch_valid(self, batch_size):
        batch_valid = random.sample(self.valid_triple, batch_size)

        return batch_valid

    def next_batch_eval(self, triplet_evaluate):
        (h, r, t) = triplet_evaluate

        batch_predict_head_raw = [triplet_evaluate]
        batch_predict_head_filter = [triplet_evaluate]
        set_head_corrupted_raw = set(self.ent_id) - set([h])
        set_head_corrupted_filter = set(self.ent_id) - set(self.dict_r_t[r][t])
        batch_predict_head_raw.extend([(head, r, t) for head in set_head_corrupted_raw])
        batch_predict_head_filter.extend([(head, r, t) for head in set_head_corrupted_filter])

        batch_predict_tail_raw = [triplet_evaluate]
        batch_predict_tail_filter = [triplet_evaluate]
        set_tail_corrupted_raw = set(self.ent_id) - set([t])
        set_tail_corrupted_filter = set(self.ent_id) - set(self.dict_r_h[r][h])
        batch_predict_tail_raw.extend([(h, r, tail) for tail in set_tail_corrupted_raw])
        batch_predict_tail_filter.extend([(h, r, tail) for tail in set_tail_corrupted_filter])

        return batch_predict_head_raw, batch_predict_head_filter, batch_predict_tail_raw, batch_predict_tail_filter

    def attention(self, entity_id):
        pass
		
    def get_img_data(self):
        image_vec = []

        for i in range(self.num_entity):
            [start_index, end_index] = self.img_index_range[i]
            if self.img_vec == 'random':
                image_vec.append(self.img_data[random.randint(int(start_index), int(end_index)+1)])
            elif self.img_vec == 'average':
                image_vec.append(np.mean(self.img_data[int(start_index): int(end_index)+1], axis=0).tolist())
            elif self.img_vec == 'attention':
                pass

        return image_vec

    def get_txt_data(self):
        txt_vec = [self.txt_data.docvecs['SENT_'+str(i)].tolist() for i in range(self.num_entity)]

        return txt_vec


