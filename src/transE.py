''' 
 transE-ae

 transE.py:
     model class
'''

import tensorflow as tf
import math
from sklearn.preprocessing import normalize

class TransE(object):

    def __init__(self, config):
        
        self.num_epoch = config.num_epoch
        self.batch_size = config.batch_size
        self.learning_rate = config.learning_rate
        self.margin = config.margin
        self.dimension = config.dimension
        self.L1_norm = config.L1_norm
        self.verbosity = config.verbosity
        self.optimizer = config.optimizer
        self.evaluate_size = config.evaluate_size
        self.weight_loss = config.weight_loss
		
        # 1st layer num features for image
        self.num_hidden_1_img = config.num_hidden_1_img
        # 1st layer num feartures for text
        self.num_hidden_1_txt = config.num_hidden_1_txt
        # 2nd layer num features (the latent dim)
        self.num_hidden_2 = config.dimension
        # image data input (features get from vgg)
        self.num_img_input = config.num_img_input
        # text data input (vectors get from doc2vec, can change to 300)
        self.num_txt_input = config.num_txt_input

    def encoder(self, x_m1, x_m2):
        with tf.variable_scope('autoencoder', reuse=True):
            weights = {
                'encoder_m1_h1': tf.get_variable(name='encoder_m1_h1'),
                'encoder_m2_h1': tf.get_variable(name='encoder_m2_h1'),
                'encoder_h1_h2': tf.get_variable(name='encoder_h1_h2')
            }
			
            biases = {
                'encoder_m1_b1': tf.get_variable(name='encoder_m1_b1'),
                'encoder_m2_b1': tf.get_variable(name='encoder_m2_b1'),
                'encoder_b2': tf.get_variable(name='encoder_b2')
            }
			
        layer_m1_h1 = tf.nn.sigmoid(tf.add(tf.matmul(x_m1, weights['encoder_m1_h1']), biases['encoder_m1_b1']))
        layer_m2_h1 = tf.nn.sigmoid(tf.add(tf.matmul(x_m2, weights['encoder_m2_h1']), biases['encoder_m2_b1']))
        layer_1 = tf.concat([layer_m1_h1, layer_m2_h1], 1)
        layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['encoder_h1_h2']), biases['encoder_b2']))
        return layer_2
			
    def decoder(self, x):
        with tf.variable_scope('autoencoder', reuse=True):
            weights = {
                'decoder_m1_h1': tf.get_variable(name='decoder_m1_h1'),
                'decoder_m2_h1': tf.get_variable(name='decoder_m2_h1'),
                'decoder_m1_h2': tf.get_variable(name='decoder_m1_h2'),
                'decoder_m2_h2': tf.get_variable(name='decoder_m2_h2')
            }
			
            biases = {
                'decoder_m1_b1': tf.get_variable(name='decoder_m1_b1'),
                'decoder_m2_b1': tf.get_variable(name='decoder_m2_b1'),
                'decoder_m1_b2': tf.get_variable(name='decoder_m1_b2'),
                'decoder_m2_b2': tf.get_variable(name='decoder_m2_b2')
            }
			
        layer_m1_h1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['decoder_m1_h1']), biases['decoder_m1_b1']))
        layer_m2_h1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['decoder_m2_h1']), biases['decoder_m2_b1']))
        layer_m1_output = tf.nn.sigmoid(tf.add(tf.matmul(layer_m1_h1, weights['decoder_m1_h2']),biases['decoder_m1_b2']))
        layer_m2_output = tf.nn.sigmoid(tf.add(tf.matmul(layer_m2_h1, weights['decoder_m2_h2']),biases['decoder_m2_b2']))
        return layer_m1_output, layer_m2_output

    def get_dissimilarity(self, batch_data):
        with tf.variable_scope('rel_embedding', reuse=True):
            embedding_relation = tf.get_variable(name='relation')
        embedding_img = tf.get_default_graph().get_tensor_by_name('ent_embedding/img:0')
        embedding_txt = tf.get_default_graph().get_tensor_by_name('ent_embedding/txt:0')
    
        embedding_relation = tf.nn.embedding_lookup(embedding_relation, batch_data[:, 1])
        batch_m1_e1 = tf.nn.embedding_lookup(embedding_img, batch_data[:, 0])
        batch_m2_e1 = tf.nn.embedding_lookup(embedding_txt, batch_data[:, 0])
        batch_m1_e2 = tf.nn.embedding_lookup(embedding_img, batch_data[:, 2])
        batch_m2_e2 = tf.nn.embedding_lookup(embedding_txt, batch_data[:, 2])
     
        bound_low = 0.05 * tf.ones([1, self.dimension])
        bound_high = 0.95 * tf.ones([1, self.dimension])

        embedding_head = self.encoder(batch_m1_e1, batch_m2_e1)
        embedding_tail = self.encoder(batch_m1_e2, batch_m2_e2)

        head_more = tf.clip_by_value(tf.abs(embedding_head)-bound_high, clip_value_min=0, clip_value_max=1)
        head_less = tf.clip_by_value(tf.abs(embedding_head)-bound_low, clip_value_min=-1, clip_value_max=0)
        tail_more = tf.clip_by_value(tf.abs(embedding_tail)-bound_high, clip_value_min=0, clip_value_max=1)
        tail_less = tf.clip_by_value(tf.abs(embedding_tail)-bound_low, clip_value_min=-1, clip_value_max=0)

        rel_more = tf.clip_by_value(tf.abs(embedding_relation)-bound_high, clip_value_min=0, clip_value_max=10000)
        rel_less = tf.clip_by_value(tf.abs(embedding_relation)-bound_low, clip_value_min=-10000, clip_value_max=0)
        
        loss_bound = head_more + tail_more - head_less - tail_less
         

        if self.L1_norm:
            dis_s = 5 * tf.reduce_mean(tf.abs(embedding_head + embedding_relation - embedding_tail), axis = 1)
            dis_l = tf.reduce_mean(10 * loss_bound, axis = 1)
            l2_norm = tf.reduce_mean(tf.pow(embedding_head, 2), axis = 1) + tf.reduce_mean(tf.pow(embedding_tail, 2), axis = 1)
            dissimilarity = dis_s + dis_l + 0.1 * l2_norm 
        else: # L2
            dis_s = tf.reduce_mean(tf.pow(embedding_head + embedding_relation - embedding_tail, 2), axis = 1)
            dis_l = 100 * tf.reduce_mean(tf.pow(loss_bound, 2), axis = 1)
            l2_norm = tf.reduce_mean(tf.pow(embedding_head, 2), axis = 1) + tf.reduce_mean(tf.pow(embedding_tail, 2), axis = 1)
            dissimilarity = dis_s + dis_l + 0.1 * l2_norm
        
        return dissimilarity, dis_s, dis_l, l2_norm
		
    def get_ae_loss(self, batch_data):
        with tf.variable_scope('rel_embedding', reuse=True):
            # embedding_entity = tf.get_variable(name='entity')
            embedding_relation = tf.get_variable(name='relation')
        embedding_img = tf.get_default_graph().get_tensor_by_name('ent_embedding/img:0')
        embedding_txt = tf.get_default_graph().get_tensor_by_name('ent_embedding/txt:0')
        
        embedding_relation = tf.nn.embedding_lookup(embedding_relation, batch_data[:, 1])
        batch_m1_e1 = tf.nn.embedding_lookup(embedding_img, batch_data[:, 0])
        batch_m2_e1 = tf.nn.embedding_lookup(embedding_txt, batch_data[:, 0])
        batch_m1_e2 = tf.nn.embedding_lookup(embedding_img, batch_data[:, 2])
        batch_m2_e2 = tf.nn.embedding_lookup(embedding_txt, batch_data[:, 2])

        layer_m1_output_e1, layer_m2_output_e1 = self.decoder(self.encoder(batch_m1_e1, batch_m2_e1))
        layer_m1_output_e2, layer_m2_output_e2 = self.decoder(self.encoder(batch_m1_e2, batch_m2_e2))

        e1_img_loss = tf.reduce_mean(tf.pow(batch_m1_e1 - layer_m1_output_e1, 2), 1)
        e1_txt_loss = tf.reduce_mean(tf.pow(batch_m2_e1 - layer_m2_output_e1, 2), 1)
		
        e2_img_loss = tf.reduce_mean(tf.pow(batch_m1_e2 - layer_m1_output_e2, 2), 1)
        e2_txt_loss = tf.reduce_mean(tf.pow(batch_m2_e2 - layer_m2_output_e2, 2), 1)
		
        loss = e1_img_loss + e1_txt_loss + e2_img_loss + e2_txt_loss

        return loss
        
    def inference(self, batch_data_pos, batch_data_neg):
        d_positive, d_s_p, d_l_p, l2_norm_p = self.get_dissimilarity(batch_data_pos)
        d_negative, d_s_n, d_l_n, l2_norm_n = self.get_dissimilarity(batch_data_neg)
        d_ae_pos = self.get_ae_loss(batch_data_pos)
        d_ae_neg = self.get_ae_loss(batch_data_neg)
        return d_positive, d_negative, d_ae_pos, d_ae_neg, d_s_p, d_l_p, l2_norm_p, d_s_n, d_l_n, l2_norm_n

    def loss(self, d_positive, d_negative, d_ae_pos, d_ae_neg, d_s_p, d_l_p, l2_norm_p, d_s_n, d_l_n, l2_norm_n):
        structure_loss = tf.maximum(0., d_s_p + self.margin - d_s_n)
        clip_loss = d_l_p + d_l_n
        norm_loss = l2_norm_p + l2_norm_n
        ae_loss = d_ae_pos + d_ae_neg
        loss = tf.reduce_mean(4 * structure_loss + clip_loss + norm_loss +  0.1 * ae_loss) # L1

        return structure_loss, clip_loss, norm_loss, ae_loss, loss
	
    def train(self, loss):
        tf.summary.scalar(loss.op.name, loss)

        if self.optimizer == 'gradient':
            optimizer = tf.train.GradientDescentOptimizer(learning_rate = self.learning_rate)
        elif self.optimizer == 'rms':
            optimizer = tf.train.RMSPropOptimizer(learning_rate = self.learning_rate)
        elif self.optimizer == 'adam':
            optimizer = tf.train.AdamOptimizer(learning_rate = self.learning_rate)
        else:
            raise NotImplementedError("Dose not support %s optimizer" % self.optimizer)

        train_op = optimizer.minimize(loss)
        return train_op

    def evaluate_dissimilarity(self, batch_data):
        with tf.variable_scope('rel_embedding', reuse=True):
            embedding_relation = tf.get_variable(name='relation')
        embedding_img = tf.get_default_graph().get_tensor_by_name('ent_embedding/img:0')
        embedding_txt = tf.get_default_graph().get_tensor_by_name('ent_embedding/txt:0')
    
        embedding_relation = tf.nn.embedding_lookup(embedding_relation, batch_data[:, 1])
        batch_m1_e1 = tf.nn.embedding_lookup(embedding_img, batch_data[:, 0])
        batch_m2_e1 = tf.nn.embedding_lookup(embedding_txt, batch_data[:, 0])
        batch_m1_e2 = tf.nn.embedding_lookup(embedding_img, batch_data[:, 2])
        batch_m2_e2 = tf.nn.embedding_lookup(embedding_txt, batch_data[:, 2])

        embedding_head = self.encoder(batch_m1_e1, batch_m2_e1)
        embedding_tail = self.encoder(batch_m1_e2, batch_m2_e2)

        if self.L1_norm:
            dissimilarity = tf.reduce_mean(tf.abs(embedding_head + embedding_relation - embedding_tail), axis = 1)
        else: # L2
            dissimilarity = tf.reduce_mean((embedding_head + embedding_relation - embedding_tail)**2, axis = 1)
        
        return dissimilarity
        
    def evaluate(self, data_predict_head, data_predict_tail):
        prediction_head = self.evaluate_dissimilarity(data_predict_head)
        prediction_tail = self.evaluate_dissimilarity(data_predict_tail)
        return prediction_head, prediction_tail
        
